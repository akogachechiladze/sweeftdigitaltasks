import java.util.*


//task1
fun isPalindrome(string: String): String {
    val stringBuilder = StringBuilder(string)
    val reverseString = stringBuilder.reverse().toString()
    return if (string == reverseString) {
        "This string is palindrome"
    } else
        "This string is not palindrome"
}


//task2
fun minCoins(amount: Int): String {
    var amount1 = amount
    val coins = mutableListOf<Int>(1, 5, 10, 20, 50)
    val length = coins.size
    val result = mutableListOf<Int>()
    for (i in length - 1 downTo 0) {
        while (amount1 >= coins[i]) {
            amount1 -= coins[i]
            result.add(coins[i])
        }
    }
    var quantity = 0
    var finalCoins = ""
    for (i in 0 until result.size) {
        quantity += 1
        finalCoins += "${result.elementAt(i)},"
    }
    return "MInimal quantity of coins is $quantity and they are ${finalCoins}"
}

//task3
fun notContains(array: Array<Int>): Int {
    val lastNumber = array.maxOrNull()
    if (lastNumber != null && lastNumber != 0) {
        for (i in 1..lastNumber) {
            return if (i !in array) {
                i
            } else {
                lastNumber + 1
            }
        }
    }
    return 1
}

//task4
fun isProperly(sequence: String): Boolean {
    var checker = 0
    for (i in sequence){
        when (i.toString()) {
            "(" -> checker+=1
            ")" -> checker-=1
        }
        if (checker<0) {
            return false
        }
    }
    return checker == 0
}

//task 5
var hasEnteredRecursion = false
fun countVariants(stearsCount: Int): Int{
    if (!hasEnteredRecursion && stearsCount == 0)
        return 0
    hasEnteredRecursion = true
    if (stearsCount == 0 || stearsCount == 1)
        return 1
    return countVariants(stearsCount - 1) + countVariants(stearsCount - 2)
}

//task6
internal class customDataStruct(dataList: ArrayList<Int?>) {
    private val positionMap = HashMap<Int?, Int>()
    private val data: ArrayList<Int?>
    fun deleteElement(value: Int) {
        if (!positionMap.containsKey(value)) return
        val position = positionMap[value]!!
        Collections.swap(data, position, data.size - 1)
        positionMap[data[position]] = position
        data.removeAt(data.size - 1)
        if (data.contains(value)) {
            positionMap[value] = data.indexOf(value)
        } else {
            positionMap.remove(value)
        }
    }

    fun getData(): ArrayList<Int?> {
        return data
    }

    init {
        data = dataList
        for (i in dataList.indices) {
            positionMap[dataList[i]] = i
        }
    }
}